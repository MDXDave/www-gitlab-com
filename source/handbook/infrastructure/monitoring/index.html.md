---
layout: markdown_page
title: "Monitoring the Infrastructure"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Monitoring

We do monitoring with Prometheus, leveraging available exporters like the node
or the postgresql exporters, and we build whatever else is necessary within
production engineering itself. The production team maintains the infrastructure
for 2 monitoring interfaces, but almost anyone can add new monitoring. The two
interfaces are:

### [Public monitoring infrastructure](http://monitor.gitlab.net/)

  - No authentication is required
  - Automatically syncs from the private monitoring infrastructure on every
  chef client execution. **Don't change dashboards here, they will be overwritten.**
  - Refer to this interface by default; only use the private one for those cases
  where the public dashboard is not available.

### [Private monitoring infrastructure](https://performance.gitlab.net)

  - Highly Available setup
  - Alerting feeds from this setup
  - Toggle "public" to have the dashboard appear on the Public monitoring
  infrastructure.
  - **Default to making public, unless you can specify a good
  reason not to**.
  - Private GitLab account is required to access
  - Separated from the public for security and availability reasons, they
  should have exactly the same graphs after we deprecate InfluxDB

## Useful System Health Dashboards

### Blackbox Monitoring

* [GitLab Web Status](http://performance.gitlab.net/dashboard/db/gitlab-web-status): front end perspective of GitLab. Useful to understand how GitLab.com looks from the user perspective. Use this graph to quickly troubleshoot what part of GitLab is slow.
* [GitLab Git Status](http://performance.gitlab.net/dashboard/db/gitlab-git-status): front end perspective of GitLab ssh access.

### Public Whitebox Monitoring

* [Fleet overview](http://monitor.gitlab.net/dashboard/db/fleet-overview): useful to see the fleet status from the inside of GitLab.com. Use this graph to quickly see if the workers or the database are under heavy load, and to check load balancer bandwidth.
* [Postgres Stats](http://monitor.gitlab.net/dashboard/db/postgres-stats): useful to understand how is the database behaving in depth. Use this graph to review if we have spikes of exclusive locks, active or idle in transaction processes
* [Postgres Queries](http://monitor.gitlab.net/dashboard/db/postgres-queries) use this dashboard to understand if we have blocked or slow queries, dead tuples, etc.
* [Storage Stats](http://monitor.gitlab.net/dashboard/db/storage-stats) use this dashboard to understand storage use and performance.

### Private Whitebox Monitor

* [Host Stats](http://performance.gitlab.net/dashboard/db/host-stats): useful to dive deep into a specific host to understand what is going on with it. Select a host from the dropdown on the top.
* [Business Stats](http://performance.gitlab.net/dashboard/db/business-stats): shows many pushes, new repos and CI builds.
* [Daily overview](http://performance.gitlab.net/dashboard/db/daily-overview): shows endpoints with amount of calls and performance metrics. Useful to understand what is slow generally.
