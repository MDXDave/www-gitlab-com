title: "Jenkins vs GitLab CI"
pdf: gitlab-vs-jenkins.pdf
competitor_one:
  name: 'GitLab'
  logo: '/images/comparison/gitlab-logo.svg'
competitor_two:
  name: 'Jenkins'
  logo: '/images/comparison/jenkins-logo.svg'
last_updated: 'May 11, 2017'
features:
  - title: "No installation needed"
    description: |
      GitLab CI ships with GitLab and is ready to use out of the box. The only
      thing you need to install is the GitLab Runner which processes the jobs.
    link_description: "Learn more about setting up the GitLab Runners."
    link: 'https://docs.gitlab.com/ce/ci/runners/'
    competitor_one: true
    competitor_two: false
  - title: "Minimal build configuration"
    description: |
      GitLab CI requires less configuration for your builds than a similar
      Jenkins setup. Your pipelines are defined in a versioned controlled
      yaml file that is easy to edit and extend while Jenkins uses a JSON-style
      format.
    link_description: "Learn more about .gitlab-ci.yaml."
    link: https://docs.gitlab.com/ce/ci/yaml/
    competitor_one: true
    competitor_two: false
  - title: "Extensible"
    description: |
      Both GitLab CI and Jenkins are extensible through an API or plugin system
      respectively, but GitLab's core CI feature set is more comprehensive than
      Jenkins.
    link_description: "Learn more about GitLab's API."
    link: 'https://docs.gitlab.com/ce/api/'
    competitor_one: true
    competitor_two: true
  - title: "Multi-platform support"
    description: |
      You can set up your own GitLab Runner on virtually any Operating System
      and any Unix distribution, from Windows to FreeBSD and Kubernetes.
    link_description: "Learn how to install and use your own Runner."
    link: 'https://docs.gitlab.com/runner/install/'
    competitor_one: true
    competitor_two: true
  - title: "Built for containers and Docker"
    description: |
      GitLab ships with its own Container Registry, CI Runner and is ready for
      a complete CI/CD container workflow. You can use any Docker image you like
      (even from private Container Registries) which means you can test and build
      on any programming language and framework.
    link_description: "Learn how to use Docker with GitLab CI."
    link: 'https://docs.gitlab.com/ce/ci/docker/using_docker_images.html'
    competitor_one: true
    competitor_two: false
  - title: "Cloud Native"
    description: |
      GitLab CI/CD is Cloud Native, purpose built for the cloud model. GitLab
      ships with Red Hat OpenShift and Kubernetes support out of the box.
    link_description: "Learn how to integrate Kubernetes with GitLab."
    link: 'https://docs.gitlab.com/ce/user/project/integrations/kubernetes.html'
    competitor_one: true
    competitor_two: false
  - title: "Scalable infrastructure built on Docker machine"
    description: |
      Thanks to GitLab Runners being able to autoscale, your infrastructure can
      contain only as much build instances as necessary at anytime.
      Autoscale provides the ability to utilize resources in a more elastic and
      dynamic way. When this feature is enabled and configured properly, jobs are
      executed on machines created on demand something that also leads to reduced
      costs.
    link_description: "Learn more about configuring autoscaling"
    link: 'https://docs.gitlab.com/runner/configuration/autoscale.html'
    competitor_one: true
    competitor_two: false
  - title: "Out of the box setup of build environments"
    description: |
      When you run GitLab on-premises, you must set up your own build environment
      by manually installing and configuring GitLab Runner.
      <a href = "/gitlab-com/">GitLab.com</a> offers
      container-based shared Runners which you can use for your projects by
      choosing any Docker image you want. Installing the appropriate software
      for your project is up to you.
    link_description: "Learn more about setting up the GitLab Runners."
    link: 'https://docs.gitlab.com/ce/ci/runners/'
    competitor_one: sortof
    competitor_two: true
  - title: "Environments and deployments"
    description: |
      GitLab CI is capable of not only testing or building your projects, but
      also deploying them in your infrastructure, with the added benefit of
      giving you a way to track and rollback your deployments. You can always
      know what is currently being deployed or has been deployed on your servers.
    link_description: "Learn more about environments and deployments."
    link: 'https://docs.gitlab.com/ee/ci/environments.html'
    competitor_one: true
    competitor_two: true
  - title: "Preview your changes with Review Apps"
    description: |
      With GitLab CI you can create a new environment for each one of your
      branches, speeding up your development process. Spin up dynamic environments
      for your merge requests with the ability to preview  your branch in a live
      environment.
    link_description: "Learn more about Review Apps."
    link: 'https://about.gitlab.com/features/review-apps/'
    competitor_one: true
    competitor_two: false
  - title: "Comprehensive pipeline graphs"
    description: |
      Pipelines can be complex structures with many sequential and parallel jobs.
      To make it a little easier to see what is going on, you can view a graph of
      a single pipeline and its status. Jenkins provides that ability only with
      an addition of a plugin.
    link_description: "Learn more about pipeline graphs."
    link: 'https://docs.gitlab.com/ee/ci/pipelines.html#pipeline-graphs'
    competitor_one: true
    competitor_two: true
  - title: "Browsable artifacts"
    description: |
      With GitLab CI you can upload your job artifacts in GitLab itself without
      the need of an external service. Because of this, artifacts are also
      browsable through GitLabs's web interface. Travis CI relies on having an
      AWS S3 account in order to upload artifacts.
    link_description: "Learn more about using job artifacts in your project."
    link: 'https://docs.gitlab.com/ce/user/project/pipelines/job_artifacts.html'
    competitor_one: true
    competitor_two: false
  - title: "Scheduled triggering of pipelines"
    description: |
      You can make your pipelines run on a schedule in a cron-like environment.
    link_description: "Learn how to trigger pipelines on a schedule in GitLab."
    link: 'https://docs.gitlab.com/ee/ci/triggers/#using-scheduled-triggers'
    competitor_one: true
    competitor_two: true
  - title: "Environment monitoring"
    description: |
      GitLab ships with Prometheus allowing you to monitor the performance of
      your deployed environments in Kubernetes without any additional
      setup.
    link_description: "Read more about GitLab's integration with Prometheus."
    link: 'https://docs.gitlab.com/ce/user/project/integrations/prometheus.html'
    competitor_one: true
    competitor_two: false
  - title: "Integrates with Cycle Analytics"
    description: |
      GitLab CI integrates with GitLab Cycle Analytics allowing you see how
      much of your cycle time from idea to production is spent on development,
      testing and review.
    link_description: "Learn more about Cycle Analytics."
    link: 'https://docs.gitlab.com/ce/user/project/cycle_analytics.html'
    competitor_one: true
    competitor_two: false
  - title: "Monitor Kubernetes deployments with Deploy Boards"
    description: |
      <a href = "/gitlab-ee/">GitLab Enterprise Edition Premium</a>
      ships with Deploy Boards offering a consolidated view of the current health
      and status of each CI environment running on Kubernetes. The status of each
      pod of your latest deployment is displayed seamlessly within GitLab without
      the need to access Kubernetes.
    link_description: "Learn more about Deploy Boards."
    link: 'https://docs.gitlab.com/ee/user/project/deploy_boards.html'
    competitor_one: true
    competitor_two: false
  - title: "Support for Canary Deployments monitoring"
    description: |
      <a href = "/gitlab-ee/">GitLab Enterprise Edition Premium</a>
      can monitor your Canary Deployments when deploying your applications with
      Kubernetes.
    link_description: "Learn more about configuring Canary Deployments."
    link: 'https://docs.gitlab.com/ee/user/project/deploy_boards.html#canary-deployments'
    competitor_one: true
    competitor_two: false
